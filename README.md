# Symmetra with Shield Generator

aka an excuse for me to learn how to do stuff in Workshop.  
Brings back Sym's Shield Generator  

## Issues

DO NOT change teams after creating a shield gen, or you won't be able to create a new one.

## Links

[GitLab page](https://gitlab.com/fnrir/symmetra-with-shield-generator)
[Workshop.codes page](https://workshop.codes/symmetra-shield-gen)
